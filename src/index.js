import "./assets/styles/index.scss";
import NavMenu from "./components/Navmenu";
import Slider from "./components/Slider";
import ShowcaseAndMenu from "./components/Showcase_And_Menu";
import Footer from "./components/Footer";
import { ModalLogUp, ModalLogIn } from "./components/Navmenu/logButtons";

const app = document.querySelector("#root");
app.append(NavMenu());
app.append(Slider());
app.append(ShowcaseAndMenu());
app.append(Footer());
app.append(ModalLogUp());
app.append(ModalLogIn());

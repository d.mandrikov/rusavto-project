import { LeftMenu } from "./menu";
import { Content } from "./showcase";

function ShowcaseAndMenu() {
  const showcaseAndMenu = document.createElement("div");
  showcaseAndMenu.className = "showcase-and-menu";
  showcaseAndMenu.id = "mainContent";
  showcaseAndMenu.append(LeftMenu());
  showcaseAndMenu.append(Content());

  return showcaseAndMenu;
}

export default ShowcaseAndMenu;

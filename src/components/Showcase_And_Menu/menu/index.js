import "./style.scss";
import { Button } from "../../Navmenu";

export function LeftMenu() {
  const leftMenu = document.createElement("div");
  leftMenu.className = "left";
  const btn1 = Button("Главная");
  btn1.className = "main-btn";
  leftMenu.prepend(btn1);

  const btn2 = Button("Легковые автомобили");
  btn2.className = "cars-btn";
  btn1.after(btn2);

  const btn3 = Button("Грузовые автомобили");
  btn3.className = "trucks-btn";
  btn2.after(btn3);

  return leftMenu;
}

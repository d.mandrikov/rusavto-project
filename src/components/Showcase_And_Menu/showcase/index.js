import "./style.scss";
import lada1 from "../../../assets/images/cars/lada1.jpg";
import lada99 from "../../../assets/images/cars/99.jpg";
import lada7 from "../../../assets/images/cars/lada7.jpg";
import priora from "../../../assets/images/cars/priora7.jpg";
import uaz from "../../../assets/images/cars/uaz-pickup.jpg";
import xray from "../../../assets/images/cars/XRAY-cross.jpg";
import avtobus from "../../../assets/images/trucks/avtobus.jpg";
import kamaz1 from "../../../assets/images/trucks/kamaz1.jpg";
import kamaz2 from "../../../assets/images/trucks/kamaz2.jpg";
import sobol_bis from "../../../assets/images/trucks/sobol_bis.jpg";
import sobol from "../../../assets/images/trucks/sobol1.jpg";
import zil from "../../../assets/images/trucks/zil.jpg";

import { Button } from "../../Navmenu";
const itemsListCars = [
  {
    src: avtobus,
    titleText: "ГАЗ",
    aboutText: "Автомобиль для транспортировки людей.",
    rating: 2,
    price: "2 000 000р",
    type: "trucks",
  },
  {
    src: kamaz1,
    titleText: "КАМАЗ",
    aboutText: "Грузовик зарекомендовавший себя своей надежностью.",
    rating: 3.5,
    price: "250 000р",
    type: "trucks",
  },
  {
    src: kamaz2,
    titleText: "КАМАЗ",
    aboutText: "Грузовик зарекомендовавший себя своей надежностью.",
    rating: 4,
    price: "200 000р",
    type: "trucks",
  },
  {
    src: priora,
    titleText: "Lada priora",
    aboutText: "Классический седан.",
    rating: 4,
    price: "150 000р",
    type: "cars",
  },
  {
    src: uaz,
    titleText: "УАЗ",
    aboutText: "Отличный внедорожник.",
    rating: 2,
    price: "400 000р",
    type: "cars",
  },
  {
    src: lada99,
    titleText: "Lada 99",
    aboutText: "Лада кабриолет.",
    rating: 4.5,
    price: "400 000р",
    type: "cars",
  },
  {
    src: lada1,
    titleText: "Лада копейка",
    aboutText: "Классический седан, советского производства.",
    rating: 2,
    price: "150 000р",
    type: "cars",
  },
  {
    src: sobol,
    titleText: "Соболь",
    aboutText: "Маршрутное такси.",
    rating: 3.5,
    price: "600 000р",
    type: "trucks",
  },
  {
    src: lada7,
    titleText: "Лада 7",
    aboutText: "Классический седан.",
    rating: 3,
    price: "200 000р",
    type: "cars",
  },
  {
    src: sobol_bis,
    titleText: "Соболь бизнес",
    aboutText:
      "Отличный автомобиль для транспортировки среднегабаритных грузов.",
    rating: 5,
    price: "1 500 000р",
    type: "trucks",
  },
  {
    src: zil,
    titleText: "ЗИЛ",
    aboutText: "Грузовик с открытым кузовом.",
    rating: 3.5,
    price: "400 000р",
    type: "trucks",
  },
  {
    src: xray,
    titleText: "Lada Xray",
    aboutText: "Новое поколение лады.",
    rating: 4.5,
    price: "1 200 000р",
    type: "cars",
  },
];

function Card(obj) {
  const card = document.createElement("div");
  let { src, titleText, aboutText, rating, price, type } = obj;
  card.className = `${type} show`;

  const img = document.createElement("img");
  img.className = "card-img-top";
  img.src = src;
  img.id = "showcaseImg";

  const cardText = document.createElement("p");
  cardText.className = "card-title";
  cardText.innerHTML = titleText;

  const about = document.createElement("p");
  about.className = "card-text";
  about.innerHTML = aboutText;

  const ratingOfCard = document.createElement("p");
  ratingOfCard.innerHTML = `Рейтинг: ${rating}`;
  ratingOfCard.className = "card-text";

  const coast = document.createElement("p");
  coast.innerHTML = price;
  coast.className = "card-text";

  const buy = document.createElement("div");
  const button = Button("Купить");
  button.addEventListener("click", () => alert("Спасибо за покупку!"));
  buy.prepend(button);

  const card100 = document.createElement("div");
  card100.className = "card h-100";

  const cardbody = document.createElement("div");
  cardbody.className = "card-body";

  card.prepend(card100);
  card100.prepend(img);
  img.after(cardbody);

  cardbody.prepend(cardText);
  cardText.after(about);
  about.after(ratingOfCard);
  ratingOfCard.after(coast);
  coast.after(buy);

  return card;
}

export function Content() {
  const content = document.createElement("div");
  content.id = "showcase";
  content.className = "row row-cols-1 row-cols-md-3 main-showcase";

  itemsListCars.forEach(function (item) {
    const product = Card(item);
    product.classList.add("col");
    product.classList.add("mb-4");
    content.prepend(product);
  });

  return content;
}

import "./style.scss";
import slide1 from "../../assets/images/slider/slide1.jpg";
import slide2 from "../../assets/images/slider/slide2.jpg";
import slide3 from "../../assets/images/slider/slide3.jpg";

const images = [
  {
    src: slide1,
  },
  {
    src: slide2,
  },
  {
    src: slide3,
  },
];
function slide(obj) {
  let { src } = obj;
  const imageDiv = document.createElement("div");
  imageDiv.className = "carousel-item";
  imageDiv.setAttribute("data-interval", "3000");

  const img = document.createElement("img");
  img.src = src;
  img.className = "d-block w-100";
  img.id = "slideImg";

  imageDiv.append(img);

  return imageDiv;
}

function Slider() {
  const carousel = document.createElement("div");
  carousel.className = "carousel-inner";

  const mainCarousel = document.createElement("div");
  mainCarousel.className = "carousel slide";
  mainCarousel.id = "carouselExampleInterval";
  mainCarousel.setAttribute("data-ride", "carousel");

  images.forEach((item) => {
    const elem = slide(item);
    carousel.append(elem);
  });
  const firstSlide = carousel.querySelector(".carousel-item");
  firstSlide.classList.add("active");

  const prevArrow = document.createElement("a");
  prevArrow.className = "carousel-control-prev";
  prevArrow.href = "#carouselExampleInterval";
  prevArrow.role = "button";
  prevArrow.setAttribute("data-slide", "prev");

  const prevSpan1 = document.createElement("span");
  prevSpan1.className = "carousel-control-prev-icon";
  prevSpan1.setAttribute("aria-hidden", "true");

  const prevSpan2 = document.createElement("span");
  prevSpan2.className = "sr-only";
  prevSpan2.innerHTML = "Previous";

  prevArrow.append(prevSpan1);
  prevArrow.append(prevSpan2);

  const nextArrow = document.createElement("a");
  nextArrow.className = "carousel-control-next";
  nextArrow.href = "#carouselExampleInterval";
  nextArrow.role = "button";
  nextArrow.setAttribute("data-slide", "next");

  const nextSpan1 = document.createElement("span");
  nextSpan1.className = "carousel-control-next-icon";
  nextSpan1.setAttribute("aria-hidden", "true");

  const nextSpan2 = document.createElement("span");
  nextSpan2.className = "sr-only";
  nextSpan2.innerHTML = "Next";

  nextArrow.append(nextSpan1);
  nextArrow.append(nextSpan2);

  mainCarousel.append(carousel);
  mainCarousel.append(prevArrow);
  mainCarousel.append(nextArrow);

  return mainCarousel;
}

export default Slider;

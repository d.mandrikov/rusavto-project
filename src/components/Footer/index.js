import "./style.scss";
import { Button } from "../Navmenu";

const navFooterList = [
  {
    nameOfLink: "О машинах",
    link: "https://ru.wikipedia.org/wiki/%D0%9C%D0%B0%D1%88%D0%B8%D0%BD%D0%B0",
  },
  {
    nameOfLink: "Самые безопасные автомобили",
    link:
      "https://rg.ru/2020/10/16/samye-bezopasnye-avtomobili-do-1-mln-rublej.html",
  },
  {
    nameOfLink: "Планы российского автопрома",
    link:
      "https://rg.ru/2019/01/08/chem-poraduet-rossijskij-avtoprom-v-blizhajshie-gody.html",
  },
  {
    nameOfLink: "Форум",
    link: "http://www.autoforum.pro",
  },
];

function footerLinks(obj) {
  let { nameOfLink, link } = obj;
  let links = document.createElement("div");
  const button = Button(nameOfLink);
  button.addEventListener("click", () => window.open(link));
  links.prepend(button);
  return links;
}
function Footer() {
  const footerDiv = document.createElement("div");
  footerDiv.className = "footerDiv";
  footerDiv.id = "footer";
  navFooterList.forEach((item) => {
    let link = footerLinks(item);
    link.className = "footer-links";
    footerDiv.append(link);
  });

  const mainBtnList = document.querySelectorAll(".main-btn");
  mainBtnList.forEach((list) =>
    list.addEventListener("click", function () {
      let hidenList = document.querySelectorAll(".trucks");
      for (let i = 0; i < hidenList.length; i++) {
        if (hidenList[i].className.indexOf("hiden") != -1) {
          hidenList[i].classList.remove("hiden");
          hidenList[i].classList.add("show");
        }
      }
      hidenList = document.querySelectorAll(".cars");
      for (let i = 0; i < hidenList.length; i++) {
        if (hidenList[i].className.indexOf("hiden") != -1) {
          hidenList[i].classList.remove("hiden");
          hidenList[i].classList.add("show");
        }
      }
    })
  );

  const carsBtnList = document.querySelectorAll(".cars-btn");
  carsBtnList.forEach((list) =>
    list.addEventListener("click", function () {
      let hidenList = document.querySelectorAll(".cars");
      for (let i = 0; i < hidenList.length; i++) {
        if (hidenList[i].className.indexOf("hiden") != -1) {
          hidenList[i].classList.remove("hiden");
          hidenList[i].classList.add("show");
        }
      }
      hidenList = document.querySelectorAll(".trucks");
      for (let i = 0; i < hidenList.length; i++) {
        if (hidenList[i].className.indexOf("show") != -1) {
          hidenList[i].classList.remove("show");
          hidenList[i].classList.add("hiden");
        }
      }
    })
  );

  const trucksBtnList = document.querySelectorAll(".trucks-btn");
  trucksBtnList.forEach((list) =>
    list.addEventListener("click", function () {
      let hidenList = document.querySelectorAll(".trucks");
      for (let i = 0; i < hidenList.length; i++) {
        if (hidenList[i].className.indexOf("hiden") != -1) {
          hidenList[i].classList.remove("hiden");
          hidenList[i].classList.add("show");
        }
      }
      hidenList = document.querySelectorAll(".cars");
      for (let i = 0; i < hidenList.length; i++) {
        if (hidenList[i].className.indexOf("show") != -1) {
          hidenList[i].classList.remove("show");
          hidenList[i].classList.add("hiden");
        }
      }
    })
  );

  return footerDiv;
}

export default Footer;

import "./style.scss";
import { Button } from "../index.js";

const logUp = Button("Войти");
logUp.id = "logUp";
logUp.addEventListener("click", function () {
  const logUpDiv = document.querySelector("#logUpForm");
  logUpDiv.classList.add("show");
});
const logIn = Button("Регистрация");
logIn.id = "logIn";
logIn.addEventListener("click", function () {
  const logInDiv = document.querySelector("#logInForm");
  logInDiv.classList.add("show");
});

export function FormWrapp() {
  const formWrapp = document.createElement("div");
  formWrapp.className = "form-wrapp";
  formWrapp.append(logUp);
  formWrapp.append(logIn);

  return formWrapp;
}

export function ModalLogUp() {
  const logUpModal = document.createElement("div");
  logUpModal.className = "container-form-log-up modal";
  logUpModal.id = "logUpForm";

  const h1 = document.createElement("h1");
  h1.className = "colorInput";
  h1.innerHTML = "Вход";

  const p = document.createElement("p");
  p.className = "colorInput";
  p.innerHTML = "Введите ваши данные для входа";

  const divName = document.createElement("div");
  const inputName = document.createElement("input");
  inputName.type = "text";
  inputName.id = "nameUp";
  inputName.placeholder = "Введите ваше имя";
  divName.append(inputName);

  const divPassword = document.createElement("div");
  const inputPassword = document.createElement("input");
  inputPassword.type = "password";
  inputPassword.id = "passwordUp";
  inputPassword.placeholder = "Введите ваш пароль";
  divPassword.append(inputPassword);

  const modalBtns = document.createElement("div");
  modalBtns.className = "modal-btns";

  const logUpBtn = Button("Войти");
  logUpBtn.id = "logIn";
  logUpBtn.type = "submit";

  logUpBtn.addEventListener("click", function () {
    const name = document.querySelector("#nameUp").value;
    const password = document.querySelector("#passwordUp").value;
    const checkName = localStorage.getItem("name");
    const checkPassword = localStorage.getItem("password");
    if (name === checkName && password === checkPassword) {
      alert(`Добро пожаловать, ${name}!`);
      const logInDiv = document.querySelector("#logUpForm");
      logInDiv.classList.remove("show");
      logInDiv.classList.add("hiden");
    } else {
      alert("Введите кореектные данные");
    }
  });

  const closeBtn = Button("Отмена");
  closeBtn.id = "closeBtn";

  closeBtn.addEventListener("click", function () {
    const logUpDiv = document.querySelector("#logUpForm");
    logUpDiv.classList.remove("show");
    logUpDiv.classList.add("hiden");
  });

  modalBtns.append(logUpBtn);
  modalBtns.append(closeBtn);

  logUpModal.append(h1);
  logUpModal.append(p);
  logUpModal.append(divName);
  logUpModal.append(divPassword);
  logUpModal.append(modalBtns);
  return logUpModal;
}

export function ModalLogIn() {
  const logInModal = document.createElement("div");
  logInModal.className = "container-form-log-in modal";
  logInModal.id = "logInForm";

  const h1 = document.createElement("h1");
  h1.className = "colorInput";
  h1.innerHTML = "Регистрация";

  const p = document.createElement("p");
  p.innerHTML = "Пожалуйста, заполните эту форму, чтобы создать аккаунт";
  p.className = "colorInput";

  const divName = document.createElement("div");
  const inputName = document.createElement("input");
  inputName.type = "text";
  inputName.id = "nameIn";
  inputName.placeholder = "Введите ваше имя";
  divName.append(inputName);

  const divEmail = document.createElement("div");
  const inputEmail = document.createElement("input");
  inputEmail.type = "text";
  inputEmail.id = "email";
  inputEmail.placeholder = "Введите ваш email";
  divEmail.append(inputEmail);

  const divPassword = document.createElement("div");
  const inputPassword = document.createElement("input");
  inputPassword.type = "password";
  inputPassword.id = "passwordIn";
  inputPassword.placeholder = "Введите ваш пароль";
  divPassword.append(inputPassword);

  const modalBtns = document.createElement("div");
  modalBtns.className = "modal-btns";

  const logInBtn = Button("Регистрация");
  logInBtn.id = "logUp";
  logInBtn.type = "submit";

  logInBtn.addEventListener("click", function () {
    const name = document.querySelector("#nameIn").value;
    const email = document.querySelector("#email").value;
    const password = document.querySelector("#passwordIn").value;
    localStorage.setItem("name", name);
    localStorage.setItem("email", email);
    localStorage.setItem("password", password);
    const logInDiv = document.querySelector("#logInForm");
    logInDiv.classList.remove("show");
    logInDiv.classList.add("hiden");
  });

  const closeBtn = Button("Отмена");
  closeBtn.id = "closeBtn";

  closeBtn.addEventListener("click", function () {
    const logInDiv = document.querySelector("#logInForm");
    logInDiv.classList.remove("show");
    logInDiv.classList.add("hiden");
  });

  modalBtns.append(logInBtn);
  modalBtns.append(closeBtn);

  logInModal.append(h1);
  logInModal.append(p);
  logInModal.append(divName);
  logInModal.append(divEmail);
  logInModal.append(divPassword);
  logInModal.append(modalBtns);
  return logInModal;
}

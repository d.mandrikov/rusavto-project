import "./style.scss";
import { FormWrapp } from "./logButtons";

export function Button(text) {
  let btn = document.createElement("button");
  btn.className = "button";
  btn.innerText = text;
  return btn;
}

const navMenuList = [
  {
    nameOfLink: "Главная",
    classes: "main-btn",
  },
  {
    nameOfLink: "Легковые автомобили",
    classes: "cars-btn",
  },
  {
    nameOfLink: "Грузовые автомобили",
    classes: "trucks-btn",
  },
];
function Logo() {
  const navMenuLogoDiv = document.createElement("div");
  navMenuLogoDiv.className = "navMenuLogo";

  const navMenuLogo = document.createElement("p");
  navMenuLogo.className = "navMenuLogoText";
  navMenuLogo.innerHTML = "RUSCAR";

  navMenuLogoDiv.prepend(navMenuLogo);
  return navMenuLogoDiv;
}
const logo = Logo();

function navMenuLinks(obj) {
  let { nameOfLink, classes } = obj;
  let links = document.createElement("div");
  const button = Button(nameOfLink);
  button.className = classes;
  links.prepend(button);
  return links;
}
function NavMenu() {
  const navMenu = document.createElement("div");
  navMenu.className = "wrapp";
  navMenu.id = "header";
  navMenuList.forEach((item) => {
    let link = navMenuLinks(item);
    link.className = "navMenu-links";
    navMenu.append(link);
  });
  navMenu.prepend(logo);
  navMenu.append(FormWrapp());
  return navMenu;
}

export default NavMenu;
